#!/bin/bash
# Setup Development Project
if [ "$#" -ne 1 ]; then
    echo "Usage:"
    echo "  $0 GUID"
    exit 1
fi

GUID=$1
echo "Setting up Parks Development Environment in project ${GUID}-parks-dev"

# Code to set up the parks development project.

# To be Implemented by Student
# Setup permissions
oc policy add-role-to-user edit system:serviceaccount:$GUID-jenkins:jenkins -n $GUID-parks-dev
oc policy add-role-to-user edit system:serviceaccount:gpte-jenkins:jenkins -n $GUID-parks-dev
oc policy add-role-to-user admin system:serviceaccount:gpte-jenkins:jenkins -n $GUID-parks-dev
oc policy add-role-to-user view --serviceaccount=default -n $GUID-parks-dev

# Create MongoDB
oc new-app mongodb-persistent --param=MONGODB_DATABASE=parks --param=MONGODB_USER=mongodb --param=MONGODB_PASSWORD=mongodb --param=MONGODB_ADMIN_PASSWORD=mongodb --param=VOLUME_CAPACITY=1Gi -n ${GUID}-parks-dev

# Set up Dev Application Environment Variables in a ConfigMap
oc create configmap mlbparks-config --from-literal="APPNAME=MLB Parks (Dev)" --from-literal="DB_HOST=mongodb" --from-literal="DB_PORT=27017" --from-literal="DB_USERNAME=mongodb" --from-literal="DB_PASSWORD=mongodb" --from-literal="DB_NAME=parks" -n ${GUID}-parks-dev
oc create configmap nationalparks-config --from-literal="APPNAME=National Parks (Dev)" --from-literal="DB_HOST=mongodb" --from-literal="DB_PORT=27017" --from-literal="DB_USERNAME=mongodb" --from-literal="DB_PASSWORD=mongodb" --from-literal="DB_NAME=parks" -n ${GUID}-parks-dev
oc create configmap parksmap-config --from-literal="APPNAME=ParksMap (Dev)" -n ${GUID}-parks-dev

# Create Binary Build
oc new-build --binary=true --name="mlbparks" jboss-eap70-openshift:1.7 -n ${GUID}-parks-dev
oc new-build --binary=true --name="nationalparks" redhat-openjdk18-openshift:1.2 -n ${GUID}-parks-dev
oc new-build --binary=true --name="parksmap" redhat-openjdk18-openshift:1.2 -n ${GUID}-parks-dev

# Create Deployment Configuration
oc new-app ${GUID}-parks-dev/mlbparks:0.0-0 --name=mlbparks --allow-missing-imagestream-tags=true -n ${GUID}-parks-dev
oc new-app ${GUID}-parks-dev/nationalparks:0.0-0 --name=nationalparks --allow-missing-imagestream-tags=true -n ${GUID}-parks-dev
oc new-app ${GUID}-parks-dev/parksmap:0.0-0 --name=parksmap --allow-missing-imagestream-tags=true -n ${GUID}-parks-dev

# Disable triggers to let building be invoked by pipeline
oc set triggers dc/mlbparks --remove-all -n ${GUID}-parks-dev
oc set triggers dc/nationalparks --remove-all -n ${GUID}-parks-dev
oc set triggers dc/parksmap --remove-all -n ${GUID}-parks-dev

# Configure environment variables from configmap
oc set env dc/mlbparks --from=configmap/mlbparks-config -n ${GUID}-parks-dev
oc set env dc/nationalparks --from=configmap/nationalparks-config -n ${GUID}-parks-dev
oc set env dc/parksmap --from=configmap/parksmap-config -n ${GUID}-parks-dev

# Add Liveness and Readiness probes
oc set probe dc/mlbparks --liveness --failure-threshold 5 --initial-delay-seconds 30 -- echo ok -n ${GUID}-parks-dev
oc set probe dc/mlbparks --readiness --failure-threshold 3 --initial-delay-seconds 60 --get-url=http://:8080/ws/healthz/ -n ${GUID}-parks-dev
oc set probe dc/nationalparks --liveness --failure-threshold 5 --initial-delay-seconds 30 -- echo ok -n ${GUID}-parks-dev
oc set probe dc/nationalparks --readiness --failure-threshold 3 --initial-delay-seconds 60 --get-url=http://:8080/ws/healthz/ -n ${GUID}-parks-dev
oc set probe dc/parksmap --liveness --failure-threshold 5 --initial-delay-seconds 30 -- echo ok -n ${GUID}-parks-dev
oc set probe dc/parksmap --readiness --failure-threshold 3 --initial-delay-seconds 60 --get-url=http://:8080/ws/healthz/ -n ${GUID}-parks-dev

# Expose service and route
oc expose dc mlbparks --port 8080 --labels="type=parksmap-backend" -n ${GUID}-parks-dev
oc expose dc nationalparks --port 8080 --labels="type=parksmap-backend" -n ${GUID}-parks-dev
oc expose dc parksmap --port 8080 -n ${GUID}-parks-dev
oc expose svc mlbparks --labels="type=parksmap-backend" -n ${GUID}-parks-dev
oc expose svc nationalparks --labels="type=parksmap-backend" -n ${GUID}-parks-dev
oc expose svc parksmap -n ${GUID}-parks-dev

oc set deployment-hook dc/mlbparks  -n ${GUID}-parks-dev --post -c mlbparks --failure-policy=retry -- curl http://mlbparks.${GUID}-parks-dev.svc.cluster.local:8080/ws/data/load/
oc set deployment-hook dc/nationalparks  -n ${GUID}-parks-dev --post -c nationalparks --failure-policy=retry -- curl http://nationalparks.${GUID}-parks-dev.svc.cluster.local:8080/ws/data/load/

# End with echo command to output exit code 0
echo "Dev environment has been setup"


