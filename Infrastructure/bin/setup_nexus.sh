#!/bin/bash
# Setup Nexus Project
if [ "$#" -ne 1 ]; then
    echo "Usage:"
    echo "  $0 GUID"
    exit 1
fi

GUID=$1
echo "Setting up Nexus in project $GUID-nexus"

# Code to set up the Nexus. It will need to
# * Create Nexus
# * Set the right options for the Nexus Deployment Config
# * Load Nexus with the right repos
# * Configure Nexus as a docker registry
# Hint: Make sure to wait until Nexus if fully up and running
#       before configuring nexus with repositories.
#       You could use the following code:
# while : ; do
#   echo "Checking if Nexus is Ready..."
#   oc get pod -n ${GUID}-nexus|grep '\-2\-'|grep -v deploy|grep "1/1"
#   [[ "$?" == "1" ]] || break
#   echo "...no. Sleeping 10 seconds."
#   sleep 10
# done

# Ideally just calls a template
# oc new-app -f ../templates/nexus.yaml --param .....

# To be Implemented by Student
# Set permissions
oc policy add-role-to-user edit system:serviceaccount:$GUID-jenkins:jenkins -n $GUID-nexus
oc policy add-role-to-user edit system:serviceaccount:gpte-jenkins:jenkins -n $GUID-nexus
oc policy add-role-to-user admin system:serviceaccount:gpte-jenkins:jenkins -n $GUID-nexus


# Create Nexus
#oc new-app -f ../templates/nexus.yaml --param=GUID=${GUID} -n ${GUID}-nexus
oc new-app -f ./Infrastructure/templates/nexus.yaml --param=GUID=${GUID} -n ${GUID}-nexus

# Wait for Nexus to become available
while : ; do
  echo "Checking if Nexus is Ready..."
  oc get pod -n ${GUID}-nexus|grep '\-1\-'|grep -v deploy|grep "1/1"
  if [ "$?" -eq "0" ]; then
    echo "Nexus is ready"
    break
  else
    echo "...no. Sleeping 10 seconds."
    sleep 10
  fi
done

# Wait for Nexus to become ready
sleep 60

# Configure repositories using script
echo "Applying permissions to execute configure Nexus script"
chmod +x ./Infrastructure/bin/configure_nexus3.sh

echo "Calling script to configure Nexus"
./Infrastructure/bin/configure_nexus3.sh admin admin123 http://$(oc get route nexus3 --template={{.spec.host}} -n ${GUID}-nexus)

# End with echo command to output exit code 0
echo "Nexus has been setup"