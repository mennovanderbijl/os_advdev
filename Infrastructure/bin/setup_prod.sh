#!/bin/bash
# Setup Production Project (initial active services: Green)
if [ "$#" -ne 1 ]; then
    echo "Usage:"
    echo "  $0 GUID"
    exit 1
fi

GUID=$1
echo "Setting up Parks Production Environment in project ${GUID}-parks-prod"

# Code to set up the parks production project. It will need a StatefulSet MongoDB, and two applications each (Blue/Green) for NationalParks, MLBParks and Parksmap.
# The Green services/routes need to be active initially to guarantee a successful grading pipeline run.

# To be Implemented by Student
# Setup permissions
oc policy add-role-to-user edit system:serviceaccount:$GUID-jenkins:jenkins -n $GUID-parks-prod
oc policy add-role-to-user edit system:serviceaccount:gpte-jenkins:jenkins -n $GUID-parks-prod
oc policy add-role-to-user admin system:serviceaccount:gpte-jenkins:jenkins -n $GUID-parks-prod
oc policy add-role-to-user view --serviceaccount=default -n $GUID-parks-prod
oc policy add-role-to-group system:image-puller system:serviceaccounts:${GUID}-parks-prod -n ${GUID}-parks-dev

# Create MongoDB StatefulSet
oc new-app -f ./Infrastructure/templates/mongodb-ss.yaml -n ${GUID}-parks-prod

# Set up Green and Blue Production Application Environment Variables in a ConfigMap
oc create configmap mlbparks-green --from-literal="APPNAME=MLB Parks (Green)" --from-literal="DB_HOST=mongodb" --from-literal="DB_PORT=27017" --from-literal="DB_USERNAME=mongodb" --from-literal="DB_PASSWORD=mongodb" --from-literal="DB_NAME=parks" -n ${GUID}-parks-prod
oc create configmap nationalparks-green --from-literal="APPNAME=National Parks (Green)" --from-literal="DB_HOST=mongodb" --from-literal="DB_PORT=27017" --from-literal="DB_USERNAME=mongodb" --from-literal="DB_PASSWORD=mongodb" --from-literal="DB_NAME=parks" -n ${GUID}-parks-prod
oc create configmap parksmap-green --from-literal="APPNAME=ParksMap (Green)" -n ${GUID}-parks-prod

oc create configmap mlbparks-blue --from-literal="APPNAME=MLB Parks (Blue)" --from-literal="DB_HOST=mongodb" --from-literal="DB_PORT=27017" --from-literal="DB_USERNAME=mongodb" --from-literal="DB_PASSWORD=mongodb" --from-literal="DB_NAME=parks" -n ${GUID}-parks-prod
oc create configmap nationalparks-blue --from-literal="APPNAME=National Parks (Blue)" --from-literal="DB_HOST=mongodb" --from-literal="DB_PORT=27017" --from-literal="DB_USERNAME=mongodb" --from-literal="DB_PASSWORD=mongodb" --from-literal="DB_NAME=parks" -n ${GUID}-parks-prod
oc create configmap parksmap-blue --from-literal="APPNAME=ParksMap (Blue)" -n ${GUID}-parks-prod

echo "Setting up Green environment"
# Create Deployment Configuration
oc new-app ${GUID}-parks-dev/mlbparks:0.0-0 --name=mlbparks-green --allow-missing-imagestream-tags=true -n ${GUID}-parks-prod
oc new-app ${GUID}-parks-dev/nationalparks:0.0-0 --name=nationalparks-green --allow-missing-imagestream-tags=true -n ${GUID}-parks-prod
oc new-app ${GUID}-parks-dev/parksmap:0.0-0 --name=parksmap-green --allow-missing-imagestream-tags=true -n ${GUID}-parks-prod

# Disable triggers to let building be invoked by pipeline
oc set triggers dc/mlbparks-green --remove-all -n ${GUID}-parks-prod
oc set triggers dc/nationalparks-green --remove-all -n ${GUID}-parks-prod
oc set triggers dc/parksmap-green --remove-all -n ${GUID}-parks-prod

# Configure environment variables from configmap
oc set env dc/mlbparks-green --from=configmap/mlbparks-green -n ${GUID}-parks-prod
oc set env dc/nationalparks-green --from=configmap/nationalparks-green -n ${GUID}-parks-prod
oc set env dc/parksmap-green --from=configmap/parksmap-green -n ${GUID}-parks-prod

# Add Liveness and Readiness probes
oc set probe dc/mlbparks-green --liveness --failure-threshold 5 --initial-delay-seconds 30 -- echo ok -n ${GUID}-parks-prod
oc set probe dc/mlbparks-green --readiness --failure-threshold 3 --initial-delay-seconds 60 --get-url=http://:8080/ws/healthz/ -n ${GUID}-parks-prod
oc set probe dc/nationalparks-green --liveness --failure-threshold 5 --initial-delay-seconds 30 -- echo ok -n ${GUID}-parks-prod
oc set probe dc/nationalparks-green --readiness --failure-threshold 3 --initial-delay-seconds 60 --get-url=http://:8080/ws/healthz/ -n ${GUID}-parks-prod
oc set probe dc/parksmap-green --liveness --failure-threshold 5 --initial-delay-seconds 30 -- echo ok -n ${GUID}-parks-prod
oc set probe dc/parksmap-green --readiness --failure-threshold 3 --initial-delay-seconds 60 --get-url=http://:8080/ws/healthz/ -n ${GUID}-parks-prod

# Expose service and route
oc expose dc mlbparks-green --port 8080 -n ${GUID}-parks-prod
oc expose dc nationalparks-green --port 8080 -n ${GUID}-parks-prod
oc expose dc parksmap-green --port 8080 -n ${GUID}-parks-prod
oc expose svc mlbparks-green --name mlbparks --labels="type=parksmap-backend" -n ${GUID}-parks-prod
oc expose svc nationalparks-green --name nationalparks --labels="type=parksmap-backend" -n ${GUID}-parks-prod
oc expose svc parksmap-green --name parksmap -n ${GUID}-parks-prod

oc set deployment-hook dc/mlbparks-green  -n ${GUID}-parks-prod --post -c mlbparks-green --failure-policy=abort -- curl http://mlbparks-green.${GUID}-parks-prod.svc.cluster.local:8080/ws/data/load/
oc set deployment-hook dc/nationalparks-green  -n ${GUID}-parks-prod --post -c nationalparks-green --failure-policy=abort -- curl http://nationalparks-green.${GUID}-parks-prod.svc.cluster.local:8080/ws/data/load/

# End with echo command to output exit code 0
echo "Green environment has been setup"

echo "Setting up Blue environment"
# Create Deployment Configuration
oc new-app ${GUID}-parks-dev/mlbparks:0.0-0 --name=mlbparks-blue --allow-missing-imagestream-tags=true -n ${GUID}-parks-prod
oc new-app ${GUID}-parks-dev/nationalparks:0.0-0 --name=nationalparks-blue --allow-missing-imagestream-tags=true -n ${GUID}-parks-prod
oc new-app ${GUID}-parks-dev/parksmap:0.0-0 --name=parksmap-blue --allow-missing-imagestream-tags=true -n ${GUID}-parks-prod

# Disable triggers to let building be invoked by pipeline
oc set triggers dc/mlbparks-blue --remove-all -n ${GUID}-parks-prod
oc set triggers dc/nationalparks-blue --remove-all -n ${GUID}-parks-prod
oc set triggers dc/parksmap-blue --remove-all -n ${GUID}-parks-prod

# Configure environment variables from configmap
oc set env dc/mlbparks-blue --from=configmap/mlbparks-blue -n ${GUID}-parks-prod
oc set env dc/nationalparks-blue --from=configmap/nationalparks-blue -n ${GUID}-parks-prod
oc set env dc/parksmap-blue --from=configmap/parksmap-blue -n ${GUID}-parks-prod

# Add Liveness and Readiness probes
oc set probe dc/mlbparks-blue --liveness --failure-threshold 5 --initial-delay-seconds 30 -- echo ok -n ${GUID}-parks-prod
oc set probe dc/mlbparks-blue --readiness --failure-threshold 3 --initial-delay-seconds 60 --get-url=http://:8080/ws/healthz/ -n ${GUID}-parks-prod
oc set probe dc/nationalparks-blue --liveness --failure-threshold 5 --initial-delay-seconds 30 -- echo ok -n ${GUID}-parks-prod
oc set probe dc/nationalparks-blue --readiness --failure-threshold 3 --initial-delay-seconds 60 --get-url=http://:8080/ws/healthz/ -n ${GUID}-parks-prod
oc set probe dc/parksmap-blue --liveness --failure-threshold 5 --initial-delay-seconds 30 -- echo ok -n ${GUID}-parks-prod
oc set probe dc/parksmap-blue --readiness --failure-threshold 3 --initial-delay-seconds 60 --get-url=http://:8080/ws/healthz/ -n ${GUID}-parks-prod

# Expose service and route
oc expose dc mlbparks-blue --port 8080 -n ${GUID}-parks-prod
oc expose dc nationalparks-blue --port 8080 -n ${GUID}-parks-prod
oc expose dc parksmap-blue --port 8080 -n ${GUID}-parks-prod
#oc expose svc mlbparks-blue --labels="type=parksmap-backend" -n ${GUID}-parks-prod
#oc expose svc nationalparks-blue --labels="type=parksmap-backend" -n ${GUID}-parks-prod
#oc expose svc parksmap-blue -n ${GUID}-parks-prod

oc set deployment-hook dc/mlbparks-blue  -n ${GUID}-parks-prod --post -c mlbparks-blue --failure-policy=retry -- curl http://mlbparks-blue.${GUID}-parks-prod.svc.cluster.local:8080/ws/data/load/
oc set deployment-hook dc/nationalparks-blue  -n ${GUID}-parks-prod --post -c nationalparks-blue --failure-policy=retry -- curl http://nationalparks-blue.${GUID}-parks-prod.svc.cluster.local:8080/ws/data/load/

# End with echo command to output exit code 0
echo "Blue environment has been setup"



