#!/bin/bash
# Setup Sonarqube Project
if [ "$#" -ne 1 ]; then
    echo "Usage:"
    echo "  $0 GUID"
    exit 1
fi

GUID=$1
echo "Setting up Sonarqube in project $GUID-sonarqube"

# Code to set up the SonarQube project.
# Ideally just calls a template
# oc new-app -f ../templates/sonarqube.yaml --param .....

# To be Implemented by Student

# Set permissions
oc policy add-role-to-user edit system:serviceaccount:$GUID-jenkins:jenkins -n $GUID-sonarqube
oc policy add-role-to-user edit system:serviceaccount:gpte-jenkins:jenkins -n $GUID-sonarqube
oc policy add-role-to-user admin system:serviceaccount:gpte-jenkins:jenkins -n $GUID-sonarqube

# Create PostgreSQL DB
oc new-app --template=postgresql-persistent --param POSTGRESQL_USER=sonar --param POSTGRESQL_PASSWORD=sonar --param POSTGRESQL_DATABASE=sonar --param VOLUME_CAPACITY=1Gi --labels=app=sonarqube_db -n $GUID-sonarqube

# Wait for PostgreSQL DB to become available
while : ; do
  echo "Checking if Postgres is Ready..."
  oc get pod -n ${GUID}-sonarqube|grep '\-1\-'|grep -v deploy|grep "1/1"
  if [ "$?" -eq "0" ]; then
    echo "Postgres is ready"
    break
  else
    echo "...no. Sleeping 10 seconds."
    sleep 10
  fi
done

echo "Deploying Sonarqube from template"
oc new-app -f ./Infrastructure/templates/sonarqube.yaml --param=GUID=${GUID} -n $GUID-sonarqube

# Wait for SonarQube to become available
while : ; do
  echo "Checking if SonarQube is Ready..."
  oc get pod -n ${GUID}-sonarqube|grep 'sonarqube\-1\-'|grep -v deploy|grep "1/1"
  if [ "$?" -eq "0" ]; then
    echo "SonarQube is ready"
    break
  else
    echo "...no. Sleeping 10 seconds."
    sleep 10
  fi
done

# End with echo command to output exit code 0
echo "SonarQube has been setup"